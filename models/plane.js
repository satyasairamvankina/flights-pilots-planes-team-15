const mongoose = require('mongoose')

const PlaneSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  tailNumber: {
    type: String,
    required: true,
    default: 'N34'
  },
  numSeats: {
    type: Number,
    required: false,
    default:3
  },
  fuelCapacity: {
    type: Number,
    required: false,
    default: 43
  }
})
module.exports = mongoose.model('Plane', PlaneSchema)
