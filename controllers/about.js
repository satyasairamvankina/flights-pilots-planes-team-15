const express = require('express')
const api = express.Router()

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

// GET t15
api.get('/t15', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t15/index.ejs',
        { title: 'TeamName', layout: 'layout.ejs' })
})
api.get('/t15/a', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('sairam',
        { title: 'Vankina satya sai ram', layout: 'layout.ejs' })
})
api.get('/t15/b', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t15/b/index.ejs',
        { title: 'Niharika Gundala', layout: 'layout.ejs' })
})
api.get('/t15/c', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t15/c/index.ejs',
        { title: 'subodh lakhinana', layout: 'layout.ejs' })
})


module.exports = api
