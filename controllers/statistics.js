const express = require('express')
const api = express.Router()
// GET to this controller base URI (the default)
api.get('/pilotstats', (req, res) => {
  //
  res.render('./Stats/pilotstats.ejs', {
    layout: 'layout.ejs',
  })
})
  api.get('/flightstats', (req, res) => {
  res.render('./Stats/flightstats.ejs', {
    layout: 'layout.ejs',
  })})
  api.get('/planestats', (req, res) => {
  res.render('./Stats/planestats.ejs', {
    layout: 'layout.ejs',
  })
})
  


module.exports = api